/* Copyright (C) 2018, 2019, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* stat.st_time support */

#include "htsky.h"
#include "htsky_c.h"
#include "htsky_atmosphere.h"
#include "htsky_cloud.h"
#include "htsky_log.h"

#include <high_tune/htcp.h>
#include <high_tune/htgop.h>
#include <high_tune/htmie.h>

#include <star/svx.h>

#include <rsys/clock_time.h>
#include <rsys/double3.h>

#include <errno.h>
#include <fcntl.h> /* open */
#include <unistd.h>
#include <sys/stat.h> /* S_IRUSR & S_IWUSR */

#include <omp.h>

/* Current version the cache structure. One should increment it and perform a
 * version management onto serialized data when the cache data data structure
 * is updated. */
static const int CACHE_VERSION = 0;

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static INLINE int
check_args(const struct htsky_args* args)
{
  return args
      && args->htgop_filename
      && args->grid_max_definition[0]
      && args->grid_max_definition[1]
      && args->grid_max_definition[2]
      && args->name
      && args->nthreads
      && args->optical_thickness >= 0
      && (unsigned)args->spectral_type < HTSKY_SPECTRAL_TYPES_COUNT__
      && args->wlen_range[0] <= args->wlen_range[1];
}

static INLINE const char*
spectral_type_string(const enum htsky_spectral_type type)
{
  const char* str = NULL;
  switch(type) {
    case HTSKY_SPECTRAL_LW: str = "longwave"; break;
    case HTSKY_SPECTRAL_SW: str = "shortwave"; break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return str;
}

static res_T
setup_bands_properties(struct htsky* sky)
{
  size_t nbands;
  size_t iband;
  res_T res = RES_OK;
  ASSERT(sky);

  nbands = htsky_get_spectral_bands_count(sky);
  ASSERT(nbands);

  sky->bands = MEM_CALLOC(sky->allocator, nbands, sizeof(*sky->bands));
  if(!sky->bands) {
    log_err(sky, "Could not allocate the list of %s band properties.\n",
      spectral_type_string(sky->spectral_type));
    res = RES_MEM_ERR;
    goto error;
  }
  FOR_EACH(iband, sky->bands_range[0], sky->bands_range[1]+1) {
    struct htgop_spectral_interval band;
    double band_wlens[2];
    const size_t i = iband - sky->bands_range[0];

    switch(sky->spectral_type) {
      case HTSKY_SPECTRAL_LW: 
        HTGOP(get_lw_spectral_interval(sky->htgop, iband, &band));
        break;
      case HTSKY_SPECTRAL_SW:
        HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));
        break;
      default: FATAL("Unreachable code.\n"); break;
    }
    band_wlens[0] = wavenumber_to_wavelength(band.wave_numbers[1]);
    band_wlens[1] = wavenumber_to_wavelength(band.wave_numbers[0]);
    ASSERT(band_wlens[0] < band_wlens[1]);

    sky->bands[i].Ca_avg = htmie_compute_xsection_absorption_average
      (sky->htmie, band_wlens, HTMIE_FILTER_LINEAR);
    sky->bands[i].Cs_avg = htmie_compute_xsection_scattering_average
      (sky->htmie, band_wlens, HTMIE_FILTER_LINEAR);
    sky->bands[i].g_avg = htmie_compute_asymmetry_parameter_average
      (sky->htmie, band_wlens, HTMIE_FILTER_LINEAR);
    ASSERT(sky->bands[i].Ca_avg > 0);
    ASSERT(sky->bands[i].Cs_avg > 0);
    ASSERT(sky->bands[i].g_avg > 0);
  }

exit:
  return res;
error:
  if(sky->bands) {
    MEM_RM(sky->allocator, sky->bands);
    sky->bands = NULL;
  }
  goto exit;
}

static INLINE double
particle_fetch_raw_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const size_t iband,
   const size_t iquad,
   const size_t ivox[3])
{
  double rho_da = 0; /* Dry air density */
  double rct = 0; /* #droplets in kg of water per kg of dry air */
  double ql = 0; /* Droplet density In kg.m^-3 */
  double Ca = 0; /* Massic absorption cross section in m^2.kg^-1 */
  double Cs = 0; /* Massic scattering cross section in m^2.kg^-1 */
  double k_particle = 0;
  size_t i = 0;
  (void)iquad;
  ASSERT(sky && ivox);

  /* Compute he dry air density */
  rho_da = cloud_dry_air_density(&sky->htcp_desc, ivox);

  /* Compute the droplet density */
  rct = htcp_desc_RCT_at(&sky->htcp_desc, ivox[0], ivox[1], ivox[2], 0);
  ql = rho_da * rct;

  i = iband - sky->bands_range[0];

  /* Use the average cross section of the current spectral band */
  if(prop == HTSKY_Ka || prop == HTSKY_Kext) Ca = sky->bands[i].Ca_avg;
  if(prop == HTSKY_Ks || prop == HTSKY_Kext) Cs = sky->bands[i].Cs_avg;

  k_particle = ql*(Ca + Cs);
  return k_particle;
}

static INLINE double
gas_fetch_raw_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const size_t iband,
   const size_t iquad,
   const int in_clouds,
   const double pos[3],
   const size_t ivox[3])
{
  struct htgop_layer layer;
  size_t ilayer = 0;
  double k_gas = 0;
  ASSERT(sky && pos && ivox);

  /* Retrieve the quadrature point into the spectral band of the layer into
   * which `pos' lies */
  HTGOP(position_to_layer_id(sky->htgop, pos[2], &ilayer));
  HTGOP(get_layer(sky->htgop, ilayer, &layer));

  if(sky->spectral_type == HTSKY_SPECTRAL_LW) {
    struct htgop_layer_lw_spectral_interval band;
    HTGOP(layer_get_lw_spectral_interval(&layer, iband, &band));

    if(!in_clouds) {
      /* Pos is outside the clouds. Directly fetch the nominal optical
       * properties */
      ASSERT(iquad < band.quadrature_length);
      switch(prop) {
        case HTSKY_Ka:
        case HTSKY_Kext:
          k_gas = band.ka_nominal[iquad];
          break;
        case HTSKY_Ks: k_gas = 0; break;
        default: FATAL("Unreachable code.\n"); break;
      }
    } else {
      /* Pos is inside the clouds. Compute the water vapor molar fraction at
       * the current voxel */
      const double x_h2o = cloud_water_vapor_molar_fraction(&sky->htcp_desc, ivox);
      struct htgop_layer_lw_spectral_interval_tab tab;

      /* Retrieve the tabulated data for the quadrature point */
      HTGOP(layer_lw_spectral_interval_get_tab(&band, iquad, &tab));

      /* Fetch the optical properties wrt x_h2o */
      switch(prop) {
        case HTSKY_Ka:
        case HTSKY_Kext:
          HTGOP(layer_lw_spectral_interval_tab_fetch_ka(&tab, x_h2o, &k_gas));
          break;
        case HTSKY_Ks: k_gas = 0; break;
        default: FATAL("Unreachable code.\n"); break;
      }
    }
  } else {
    struct htgop_layer_sw_spectral_interval band;
    ASSERT(sky->spectral_type == HTSKY_SPECTRAL_SW);

    HTGOP(layer_get_sw_spectral_interval(&layer, iband, &band));
    if(!in_clouds) {
      /* Pos is outside the clouds. Directly fetch the nominal optical
       * properties */
      ASSERT(iquad < band.quadrature_length);
      switch(prop) {
        case HTSKY_Ka: k_gas = band.ka_nominal[iquad]; break;
        case HTSKY_Ks: k_gas = band.ks_nominal[iquad]; break;
        case HTSKY_Kext:
          k_gas = band.ka_nominal[iquad] + band.ks_nominal[iquad];
          break;
        default: FATAL("Unreachable code.\n"); break;
      }
    } else {
      /* Pos is inside the clouds. Compute the water vapor molar fraction at
       * the current voxel */
      const double x_h2o = cloud_water_vapor_molar_fraction(&sky->htcp_desc, ivox);
      struct htgop_layer_sw_spectral_interval_tab tab;

      /* Retrieve the tabulated data for the quadrature point */
      HTGOP(layer_sw_spectral_interval_get_tab(&band, iquad, &tab));

      /* Fetch the optical properties wrt x_h2o */
      switch(prop) {
        case HTSKY_Ka:
          HTGOP(layer_sw_spectral_interval_tab_fetch_ka(&tab, x_h2o, &k_gas));
          break;
        case HTSKY_Ks:
          HTGOP(layer_sw_spectral_interval_tab_fetch_ks(&tab, x_h2o, &k_gas));
          break;
        case HTSKY_Kext:
          HTGOP(layer_sw_spectral_interval_tab_fetch_kext(&tab, x_h2o, &k_gas));
          break;
        default: FATAL("Unreachable code.\n"); break;
      }
    }
  }
  return k_gas;
}

static res_T
setup_cache_stream
  (struct htsky* sky,
   const char* htcp_filename,
   const char* htgop_filename,
   const char* htmie_filename,
   const char* cache_filename,
   int* out_create_cache, /* Define if the cache file was created */
   FILE** out_fp)
{
  FILE* fp = NULL;
  struct stat htcp_statbuf;
  struct stat htgop_statbuf;
  struct stat htmie_statbuf;
  int create_cache = 0;
  int fd = -1;
  res_T res = RES_OK;
  ASSERT(sky && out_create_cache && out_fp);
  ASSERT(htcp_filename && htgop_filename && htmie_filename && cache_filename);

  /* Open the cache file */
  fd = open(cache_filename, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
  if(fd >= 0) {
    create_cache = 1;
  } else if (errno == EEXIST) { /* The cache already exists */
    fd = open(cache_filename, O_RDWR, 0);
  }

  if(fd < 0) {
    log_err(sky, "Unexpected error while opening the cache file `%s'.\n",
      cache_filename);
    res = RES_IO_ERR;
    goto error;
  }

  fp = fdopen(fd, "w+");
  if(!fp) {
    log_err(sky, "Could not open the cache file `%s'.\n", cache_filename);
    res = RES_IO_ERR;
    goto error;
  }

  /* Query the status of the input */
  #define STAT(Filename, Statbuf) {                                            \
    const int err = stat(Filename, Statbuf);                                   \
    if(err) {                                                                  \
      log_err(sky, "%s: could not stat the file -- %s\n",                      \
        Filename, strerror(errno));                                            \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  STAT(htcp_filename, &htcp_statbuf);
  STAT(htgop_filename, &htgop_statbuf);
  STAT(htmie_filename, &htmie_statbuf);
  #undef STAT

  if(create_cache) {
    /* Setup the cache header, i.e. data that uniquely identify the cache
     * regarding the input files (htcp, htmie and htgop files) */
    #define WRITE(Var, N) {                                                    \
      if(fwrite((Var), sizeof(*(Var)), (N), fp) != (N)) {                      \
        log_err(sky, "%s: could not write the cache header.\n",cache_filename);\
        res = RES_IO_ERR;                                                      \
        goto error;                                                            \
      }                                                                        \
    } (void)0
    WRITE(&CACHE_VERSION, 1);
    WRITE(&htcp_statbuf.st_ino, 1);
    WRITE(&htcp_statbuf.st_mtim, 1);
    WRITE(&htgop_statbuf.st_ino, 1);
    WRITE(&htgop_statbuf.st_mtim, 1);
    WRITE(&htmie_statbuf.st_ino, 1);
    WRITE(&htmie_statbuf.st_mtim, 1);
    WRITE(&sky->spectral_type, 1);
    WRITE(sky->bands_range, 2);
    #undef WRITE
    CHK(fflush(fp) == 0);
  } else {
    struct stat htcp_statbuf2;
    struct stat htgop_statbuf2;
    struct stat htmie_statbuf2;
    int cache_version;
    enum htsky_spectral_type spectral_type;
    size_t bands_range[2];

    /* Read the cache header */
    #define READ(Var, N) {                                                     \
      if(fread((Var), sizeof(*(Var)), (N), fp) != (N)) {                       \
        if(feof(fp)) {                                                         \
          res = RES_BAD_ARG;                                                   \
        } else if(ferror(fp)) {                                                \
          res = RES_IO_ERR;                                                    \
        } else {                                                               \
          res = RES_UNKNOWN_ERR;                                               \
        }                                                                      \
        log_err(sky, "%s: could not read the cache header.\n",cache_filename); \
        goto error;                                                            \
      }                                                                        \
    } (void)0
    READ(&cache_version, 1);
    if(cache_version != CACHE_VERSION) {
      log_err(sky,
        "%s: invalid cache in version %d. Expecting a cache in version %d.\n",
        cache_filename, cache_version, CACHE_VERSION);
      res = RES_BAD_ARG;
      goto error;
    }
    READ(&htcp_statbuf2.st_ino, 1);
    READ(&htcp_statbuf2.st_mtim, 1);
    READ(&htgop_statbuf2.st_ino, 1);
    READ(&htgop_statbuf2.st_mtim, 1);
    READ(&htmie_statbuf2.st_ino, 1);
    READ(&htmie_statbuf2.st_mtim, 1);
    READ(&spectral_type, 1);
    READ(bands_range, 2);
    #undef READ

    /* Compare the cache header with the input file status to check that the
     * cached data matched the input data */
    #define CHK_STAT(Stat0, Stat1) {                                           \
      if((Stat0)->st_ino != (Stat1)->st_ino                                    \
      || (Stat0)->st_mtim.tv_sec != (Stat1)->st_mtim.tv_sec                    \
      || (Stat0)->st_mtim.tv_nsec != (Stat1)->st_mtim.tv_nsec) {               \
        log_err(sky, "%s: invalid cache regarding the input files.\n",         \
          cache_filename);                                                     \
        res = RES_BAD_ARG;                                                     \
        goto error;                                                            \
      }                                                                        \
    } (void)0
    CHK_STAT(&htcp_statbuf, &htcp_statbuf2);
    CHK_STAT(&htgop_statbuf, &htgop_statbuf2);
    CHK_STAT(&htmie_statbuf, &htmie_statbuf2);
    #undef CHK_STAT

    /* Compare the handled spectral bands with the bands to handled to check
     * that the cached octress are the expected ones */
    if(spectral_type != sky->spectral_type
    || bands_range[0] != sky->bands_range[0]
    || bands_range[1] != sky->bands_range[1]) {
      log_err(sky, "%s: invalid cache regarding the wavelengths to handle.\n",
        cache_filename);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  *out_fp = fp;
  *out_create_cache = create_cache;
  return res;
error:
  if(fp) {
    CHK(fclose(fp) == 0);
    fp = NULL;
  } else if(fd >= 0) {
    CHK(close(fd) == 0);
  }
  goto exit;
}

static void
print_spectral_info(const struct htsky* sky)
{
  struct htgop_spectral_interval band_low, band_upp;
  size_t iband_low, iband_upp;
  size_t nbands;
  size_t i;
  size_t naccels = 0;
  ASSERT(sky);

  nbands = htsky_get_spectral_bands_count(sky);

  iband_low = htsky_get_spectral_band_id(sky, 0);
  iband_upp = htsky_get_spectral_band_id(sky, nbands-1);

  /* Retrieve the spectral interval boundaries */
  switch(sky->spectral_type) {
    case HTSKY_SPECTRAL_LW:
      HTGOP(get_lw_spectral_interval(sky->htgop, iband_low, &band_low));
      HTGOP(get_lw_spectral_interval(sky->htgop, iband_upp, &band_upp));
      break;
    case HTSKY_SPECTRAL_SW:
      HTGOP(get_sw_spectral_interval(sky->htgop, iband_low, &band_low));
      HTGOP(get_sw_spectral_interval(sky->htgop, iband_upp, &band_upp));
      break;
    default: FATAL("Unreachable code.\n"); break;
  }

  log_info(sky, "Sky data defined in [%g, %g] nanometers over %lu %s.\n",
    wavenumber_to_wavelength(band_upp.wave_numbers[1]),
    wavenumber_to_wavelength(band_low.wave_numbers[0]),
    (unsigned long)nbands,
    nbands > 1 ? "bands" : "band");

  /* Compute the overall number of sky acceleration structures to build */
  FOR_EACH(i, 0, nbands) {
    struct htgop_spectral_interval band;
    const  size_t iband = htsky_get_spectral_band_id(sky, i);

    switch(sky->spectral_type) {
      case HTSKY_SPECTRAL_LW:
        HTGOP(get_lw_spectral_interval(sky->htgop, iband, &band));
        break;
      case HTSKY_SPECTRAL_SW:
        HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));
        break;
      default: FATAL("Unreachable code.\n"); break;
    }
    naccels += band.quadrature_length;
  }

  log_info(sky, "Number of clouds partitionning structures: %lu\n",
    (unsigned long)naccels);
}

static void
release_sky(ref_T* ref)
{
  struct htsky* sky;
  ASSERT(ref);
  sky = CONTAINER_OF(ref, struct htsky, ref);
  cloud_clean(sky);
  atmosphere_clean(sky);
  if(sky->svx) SVX(device_ref_put(sky->svx));
  if(sky->htcp) HTCP(ref_put(sky->htcp));
  if(sky->htgop) HTGOP(ref_put(sky->htgop));
  if(sky->htmie) HTMIE(ref_put(sky->htmie));
  if(sky->bands) MEM_RM(sky->allocator, sky->bands);
  if(sky->logger == &sky->logger__) logger_release(&sky->logger__);
  darray_split_release(&sky->svx2htcp_z);
  str_release(&sky->name);
  ASSERT(MEM_ALLOCATED_SIZE(&sky->svx_allocator) == 0);
  mem_shutdown_proxy_allocator(&sky->svx_allocator);
  MEM_RM(sky->allocator, sky);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
htsky_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* mem_allocator, /* NULL <=> use default allocator */
   const struct htsky_args* args,
   struct htsky** out_sky)
{
  struct time t0, t1;
  struct mem_allocator* allocator = NULL;
  struct htsky* sky = NULL;
  double wnums[2];
  char buf[128];
  int nthreads_max;
  int force_cache_upd = 0;
  FILE* cache = NULL;
  res_T res = RES_OK;

  if(!check_args(args) || !out_sky) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  sky = MEM_CALLOC(allocator, 1, sizeof(*sky));
  if(!sky) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the HTSky data structure.\n"
      if(logger) {
        logger_print(logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  nthreads_max = MMAX(omp_get_max_threads(), omp_get_num_procs());
  ref_init(&sky->ref);
  sky->allocator = allocator;
  sky->verbose = args->verbose;
  sky->spectral_type = args->spectral_type ;
  sky->repeat_clouds = args->repeat_clouds;
  sky->is_cloudy = args->htcp_filename != NULL;
  darray_split_init(sky->allocator, &sky->svx2htcp_z);
  str_init(sky->allocator, &sky->name);
  sky->bands_range[0] = 1;
  sky->bands_range[1] = 0;
  sky->nthreads = MMIN(args->nthreads, (unsigned)nthreads_max);

  if(logger) {
    sky->logger = logger;
  } else {
    setup_log_default(sky);
  }

  res = str_set(&sky->name, args->name);
  if(res != RES_OK) {
    log_err(sky, "Cannot setup the sky name to `%s'.\n", args->name);
    goto error;
  }

  /* Setup an allocator specific to the SVX library */
  res = mem_init_proxy_allocator(&sky->svx_allocator, sky->allocator);
  if(res != RES_OK) {
    log_err(sky, "Cannot init the allocator used to manage the Star-VX data.\n");
    goto error;
  }

  /* Create the Star-VX library device */
  res = svx_device_create
    (sky->logger, &sky->svx_allocator, sky->verbose, &sky->svx);
  if(res != RES_OK) {
    log_err(sky, "Error creating the Star-VX library device.\n");
    goto error;
  }

  /* Load the gas optical properties */
  res = htgop_create(sky->logger, sky->allocator, sky->verbose, &sky->htgop);
  if(res != RES_OK) {
    log_err(sky, "Could not create the gas optical properties loader.\n");
    goto error;
  }
  res = htgop_load(sky->htgop, args->htgop_filename);
  if(res != RES_OK) {
    log_err(sky, "Error loading the gas optical properties -- `%s'.\n",
      args->htgop_filename);
    goto error;
  }

  /* Retrieve the spectral bands */
  wnums[0] = wavelength_to_wavenumber(args->wlen_range[1]);
  wnums[1] = wavelength_to_wavenumber(args->wlen_range[0]);
  switch(sky->spectral_type) {
    case HTSKY_SPECTRAL_LW:
      res = htgop_get_lw_spectral_intervals(sky->htgop, wnums, sky->bands_range);
      break;
    case HTSKY_SPECTRAL_SW:
      res = htgop_get_sw_spectral_intervals(sky->htgop, wnums, sky->bands_range);
      break;
    default: FATAL("Unreachable code.\n"); break;
  } 
  if(res != RES_OK) goto error;
  
  print_spectral_info(sky);

  /* Setup the atmopshere */
  time_current(&t0);
  res = atmosphere_setup(sky, args->optical_thickness);
  if(res != RES_OK) goto error;
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(sky, "Setup atmosphere in %s\n", buf);

  /* Nothing more to do */
  if(!sky->is_cloudy) goto exit;

  if(!args->htmie_filename) {
    log_err(sky, "Missing the HTMie filename.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  if(!args->htcp_filename) {
    log_err(sky, "Missing the HTCP filename.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  /* Load MIE data */
  res = htmie_create(sky->logger, sky->allocator, sky->verbose, &sky->htmie);
  if(res != RES_OK) {
    log_err(sky, "Could not create the Mie's data loader.\n");
    goto error;
  }
  res = htmie_load(sky->htmie, args->htmie_filename);
  if(res != RES_OK) {
    log_err(sky, "Error loading the Mie's data -- `%s'.\n", args->htmie_filename);
    goto error;
  }

  /* Setup the properties of the Short/Long wave bands */
  res = setup_bands_properties(sky);
  if(res != RES_OK) goto error;

  /* Load clouds properties */
  res = htcp_create(sky->logger, sky->allocator, sky->verbose, &sky->htcp);
  if(res != RES_OK) {
    log_err(sky, "Could not create the loader of cloud properties.\n");
    goto error;
  }
  res = htcp_load(sky->htcp, args->htcp_filename);
  if(res != RES_OK) {
    log_err(sky, "Error loading the cloud properties -- `%s'.\n",
      args->htcp_filename);
    goto error;
  }

  if(args->cache_filename) {
    res = setup_cache_stream(sky, args->htcp_filename, args->htgop_filename,
      args->htmie_filename, args->cache_filename, &force_cache_upd, &cache);
    if(res != RES_OK) goto error;
  }

  time_current(&t0);
  res = cloud_setup(sky, args->grid_max_definition, args->optical_thickness,
    args->cache_filename, force_cache_upd, cache);
  if(res != RES_OK) goto error;
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(sky, "Setup clouds in %s\n", buf);

  if(sky->verbose) {
    log_svx_memory_usage(sky);
  }

exit:
  if(cache) fclose(cache);
  if(out_sky) *out_sky = sky;
  return res;
error:
  if(sky) {
    htsky_ref_put(sky);
    sky = NULL;
  }
  goto exit;
}

res_T
htsky_ref_get(struct htsky* sky)
{
  if(!sky) return RES_BAD_ARG;
  ref_get(&sky->ref);
  return RES_OK;
}

res_T
htsky_ref_put(struct htsky* sky)
{
  if(!sky) return RES_BAD_ARG;
  ref_put(&sky->ref, release_sky);
  return RES_OK;
}

const char*
htsky_get_name(const struct htsky* sky)
{
  ASSERT(sky);
  return str_cget(&sky->name);
}

double
htsky_fetch_particle_phase_function_asymmetry_parameter
  (const struct htsky* sky,
   const size_t ispectral_band,
   const size_t iquad)
{
  size_t i;
  ASSERT(sky);
  ASSERT(ispectral_band >= sky->bands_range[0]);
  ASSERT(ispectral_band <= sky->bands_range[1]);
  (void)iquad;
  if(!sky->is_cloudy) {
    return 0;
  } else {
    i = ispectral_band - sky->bands_range[0];
    return sky->bands[i].g_avg;
  }
}

double
htsky_fetch_per_wavelength_particle_phase_function_asymmetry_parameter
  (const struct htsky* sky,
   const double wavelength) /* In nanometer */
{
  ASSERT(sky);
  if(!sky->is_cloudy) {
    return 0;
  } else {
    return htmie_fetch_asymmetry_parameter
      (sky->htmie, wavelength, HTMIE_FILTER_LINEAR);
  }
}

double
htsky_fetch_raw_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const int components_mask, /* Combination of htsky_component_flag */
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const double pos[3],
   const double k_min,
   const double k_max)
{
  size_t ivox[3];
  size_t i;
  const struct svx_tree_desc* cloud_desc = NULL;
  const struct svx_tree_desc* atmosphere_desc = NULL;
  int comp_mask = components_mask;
  int in_clouds; /* Defines if `pos' lies in the clouds */
  int in_atmosphere; /* Defines if `pos' lies in the atmosphere */
  double pos_cs[3]; /* Position in cloud space */
  double k_particle = 0;
  double k_gas = 0;
  double k = 0;
  ASSERT(sky && pos);
  ASSERT(iband >= sky->bands_range[0]);
  ASSERT(iband <= sky->bands_range[1]);
  ASSERT(comp_mask & HTSKY_CPNT_MASK_ALL);

  i = iband - sky->bands_range[0];
  cloud_desc = sky->is_cloudy ? &sky->clouds[i][iquad].octree_desc : NULL;
  atmosphere_desc = &sky->atmosphere[i][iquad].bitree_desc;
  ASSERT(atmosphere_desc->frame[0] == SVX_AXIS_Z);

  /* Is the position inside the clouds? */
  if(!sky->is_cloudy) {
    in_clouds = 0;
  } else if(sky->repeat_clouds) {
    in_clouds =
       pos[2] >= cloud_desc->lower[2]
    && pos[2] <= cloud_desc->upper[2];
  } else {
    in_clouds =
       pos[0] >= cloud_desc->lower[0]
    && pos[1] >= cloud_desc->lower[1]
    && pos[2] >= cloud_desc->lower[2]
    && pos[0] <= cloud_desc->upper[0]
    && pos[1] <= cloud_desc->upper[1]
    && pos[2] <= cloud_desc->upper[2];
  }

  /* Is the position inside the atmosphere? */
  ASSERT(atmosphere_desc->frame[0] == SVX_AXIS_Z);
  in_atmosphere =
     pos[2] >= atmosphere_desc->lower[2]
  && pos[2] <= atmosphere_desc->upper[2];

  if(!in_clouds) {
    /* Make invalid the voxel index */
    ivox[0] = SIZE_MAX;
    ivox[1] = SIZE_MAX;
    ivox[2] = SIZE_MAX;
    /* Not in clouds => No particle */
    comp_mask &= ~HTSKY_CPNT_FLAG_PARTICLES;
    /* Not in atmopshere => No gas */
    if(!in_atmosphere) comp_mask &= ~HTSKY_CPNT_FLAG_GAS;
  } else {
    const double* upp;
    const size_t* def;
    world_to_cloud(sky, pos, pos_cs);

    /* Compute the index of the voxel to fetch */
    ivox[0] = (size_t)((pos_cs[0] - cloud_desc->lower[0])/sky->htcp_desc.vxsz_x);
    ivox[1] = (size_t)((pos_cs[1] - cloud_desc->lower[1])/sky->htcp_desc.vxsz_y);
    if(!sky->htcp_desc.irregular_z) {
      /* The voxels along the Z dimension have the same size */
      ivox[2] = (size_t)((pos_cs[2] - cloud_desc->lower[2])/sky->htcp_desc.vxsz_z[0]);
    } else {
      /* Irregular voxel size along the Z dimension. Compute the index of the Z
       * position in the svx2htcp_z Look Up Table and use the LUT to define the
       * voxel index into the HTCP descriptor */
      const struct split* splits = darray_split_cdata_get(&sky->svx2htcp_z);
      const size_t ilut = (size_t)
        ((pos_cs[2] - cloud_desc->lower[2]) / sky->lut_cell_sz);
      ivox[2] = splits[ilut].index + (pos_cs[2] > splits[ilut].height);
    }

    /* Handle numerical issues that may lead to a position lying onto the cloud
     * upper boundaries */
    def = sky->htcp_desc.spatial_definition;
    upp = cloud_desc->upper;
    if(ivox[0] == def[0] && eq_eps(pos_cs[0], upp[0], 1.e-6)) ivox[0] = def[0]-1;
    if(ivox[1] == def[1] && eq_eps(pos_cs[1], upp[1], 1.e-6)) ivox[1] = def[1]-1;
    if(ivox[2] == def[2] && eq_eps(pos_cs[2], upp[2], 1.e-6)) ivox[2] = def[2]-1;
    if(ivox[0] >= def[0]) FATAL("Out of bound X voxel coordinate\n");
    if(ivox[1] >= def[1]) FATAL("Out of bound Y voxel coordinate\n");
    if(ivox[2] >= def[2]) FATAL("Out of bound Z voxel coordinate\n");
  }

  if(comp_mask & HTSKY_CPNT_FLAG_PARTICLES) {
    k_particle = particle_fetch_raw_property(sky, prop, iband, iquad, ivox);
  }
  if(comp_mask & HTSKY_CPNT_FLAG_GAS) {
    k_gas = gas_fetch_raw_property
      (sky, prop, iband, iquad, in_clouds, pos, ivox);
  }
  k = k_particle + k_gas;
  ASSERT(k >= k_min && k <= k_max);
  (void)k_min, (void)k_max;
  return k;
}

double
htsky_fetch_temperature(const struct htsky* sky, const double pos[3])
{
  double temperature = 0;
  struct htgop_level lvl0, lvl1;
  size_t nlvls = 0;
  int in_clouds = 0;
  int in_atmosphere = 0;
  ASSERT(sky && pos);

  /* Is the position inside the atmosphere */
  HTGOP(get_levels_count(sky->htgop, &nlvls));
  ASSERT(nlvls > 1);
  HTGOP(get_level(sky->htgop, 0, &lvl0));
  HTGOP(get_level(sky->htgop, nlvls-1, &lvl1));
  in_atmosphere = pos[2] >= lvl0.height && pos[2] <= lvl1.height;

  /* Is the position inside the clouds? */
  if(!sky->is_cloudy) {
    in_clouds = 0;
  } else if(sky->repeat_clouds) {
    in_clouds =
       pos[2] >= sky->htcp_desc.lower[2]
    && pos[2] <= sky->htcp_desc.upper[2];
  } else {
    in_clouds =
       pos[0] >= sky->htcp_desc.lower[0]
    && pos[1] >= sky->htcp_desc.lower[1]
    && pos[2] >= sky->htcp_desc.lower[2]
    && pos[0] <= sky->htcp_desc.upper[0]
    && pos[1] <= sky->htcp_desc.upper[1]
    && pos[2] <= sky->htcp_desc.upper[2];
  }

  if(in_clouds) {
    /* Clouds have priority on atmosphere */
    in_atmosphere = 0;
  }

  if(in_atmosphere) {
    double u;
    size_t ilayer;

    /* Find the layer into which pos is included */
    HTGOP(position_to_layer_id(sky->htgop, pos[2], &ilayer));
    ASSERT(ilayer < nlvls-1);

    /* Fetch the levels enclosing the current layer */
    HTGOP(get_level(sky->htgop, ilayer+0, &lvl0));
    HTGOP(get_level(sky->htgop, ilayer+1, &lvl1));
    ASSERT(lvl0.height < lvl1.height);
    ASSERT(lvl0.height <= pos[2]  && pos[2] <= lvl1.height);

    /* Linearly interpolate the temperature of the levels into which pos lies */
    u = (pos[2] - lvl0.height) / (lvl1.height - lvl0.height);
    temperature = u * (lvl1.temperature - lvl0.temperature) + lvl0.temperature;

  } else if(in_clouds) {
    const struct htcp_desc* desc = &sky->htcp_desc;
    size_t ivox[3];
    double pos_cs[3];

    /* Transform the submitted position in local cloud space */
    world_to_cloud(sky, pos, pos_cs);

    /* Compute the index of the voxel to fetch */
    ivox[0] = (size_t)((pos_cs[0] - desc->lower[0])/desc->vxsz_x);
    ivox[1] = (size_t)((pos_cs[1] - desc->lower[1])/desc->vxsz_y);
    if(!desc->irregular_z) {
      /* The voxels along the Z dimension have the same size */
      ivox[2] = (size_t)((pos_cs[2] - desc->lower[2])/desc->vxsz_z[0]);
    } else {
      /* Irregular voxel size along the Z dimension. Compute the index of the Z
       * position in the svx2htcp_z Look Up Table and use the LUT to define the
       * voxel index into the HTCP descripptor */
      const struct split* splits = darray_split_cdata_get(&sky->svx2htcp_z);
      const size_t ilut = (size_t)((pos_cs[2] - desc->lower[2]) / sky->lut_cell_sz);
      ivox[2] = splits[ilut].index + (pos_cs[2] > splits[ilut].height);
    }

    /* Fetch the cloud temperature */
    temperature = htcp_desc_T_at(desc, ivox[0], ivox[1], ivox[2], 0);
  }

  return temperature;
}

size_t
htsky_get_spectral_bands_count(const struct htsky* sky)
{
  ASSERT(sky && sky->bands_range[0] <= sky->bands_range[1]);
  return sky->bands_range[1] - sky->bands_range[0] + 1;
}

size_t
htsky_get_spectral_band_id
  (const struct htsky* sky, const size_t i)
{
  ASSERT(sky);
  ASSERT(i < htsky_get_spectral_bands_count(sky));
  return sky->bands_range[0] + i;
}

size_t
htsky_get_spectral_band_quadrature_length
  (const struct htsky* sky, const size_t iband)
{
  struct htgop_spectral_interval band;
  ASSERT(sky);
  ASSERT(iband >= sky->bands_range[0]);
  ASSERT(iband <= sky->bands_range[1]);
  switch(sky->spectral_type) {
    case HTSKY_SPECTRAL_LW:
      HTGOP(get_lw_spectral_interval(sky->htgop, iband, &band));
      break;
    case HTSKY_SPECTRAL_SW:
      HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return band.quadrature_length;
}

res_T
htsky_get_spectral_band_bounds
  (const struct htsky* sky,
   const size_t iband,
   double wavelengths[2])
{
  struct htgop_spectral_interval specint;
  res_T res = RES_OK;
  ASSERT(sky && wavelengths);

  switch(sky->spectral_type) {
    case HTSKY_SPECTRAL_LW:
      res = htgop_get_lw_spectral_interval(sky->htgop, iband, &specint);
      break;
    case HTSKY_SPECTRAL_SW:
      res = htgop_get_sw_spectral_interval(sky->htgop, iband, &specint);
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  if(res != RES_OK) return res;
  wavelengths[0] = wavenumber_to_wavelength(specint.wave_numbers[1]);
  wavelengths[1] = wavenumber_to_wavelength(specint.wave_numbers[0]);
  ASSERT(wavelengths[0] < wavelengths[1]);
  return RES_OK;
}

res_T
htsky_get_raw_spectral_bounds(const struct htsky* sky, double wavelengths[2])
{
  size_t n;
  double band_first_bounds[2];
  double band_last_bounds[2];
  size_t iband_first;
  size_t iband_last;
  ASSERT(sky && wavelengths);

  n = htsky_get_spectral_bands_count(sky);

  iband_first = htsky_get_spectral_band_id(sky, 0);
  iband_last  = htsky_get_spectral_band_id(sky, n-1);

  HTSKY(get_spectral_band_bounds(sky, iband_first, band_first_bounds));
  HTSKY(get_spectral_band_bounds(sky, iband_last,  band_last_bounds));
  wavelengths[0] = MMIN(band_first_bounds[0], band_last_bounds[0]);
  wavelengths[1] = MMAX(band_first_bounds[1], band_last_bounds[1]);

  return RES_OK;
}

enum htsky_spectral_type
htsky_get_spectral_type(const struct htsky* htsky)
{
  ASSERT(htsky);
  return htsky->spectral_type;
}

size_t
htsky_find_spectral_band(const struct htsky* sky, const double wavelength)
{
  const double wnum = wavelength_to_wavenumber(wavelength);
  size_t iband;
  ASSERT(sky);
  switch(sky->spectral_type) {
    case HTSKY_SPECTRAL_LW:
      HTGOP(find_lw_spectral_interval_id(sky->htgop, wnum, &iband));
      break;
    case HTSKY_SPECTRAL_SW:
      HTGOP(find_sw_spectral_interval_id(sky->htgop, wnum, &iband));
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return iband;
}

size_t
htsky_spectral_band_sample_quadrature
  (const struct htsky* sky,
   const double r,
   const size_t iband)
{
  struct htgop_spectral_interval band;
  size_t iquad;
  ASSERT(sky);
  ASSERT(sky->bands_range[0] <= iband || iband <= sky->bands_range[1]);

  switch(sky->spectral_type) {
    case HTSKY_SPECTRAL_LW:
      HTGOP(get_lw_spectral_interval(sky->htgop, iband, &band));
      break;
    case HTSKY_SPECTRAL_SW:
      HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  HTGOP(spectral_interval_sample_quadrature(&band, r, &iquad));
  return iquad;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
double*
world_to_cloud
  (const struct htsky* sky,
   const double pos_ws[3], /* World space position */
   double out_pos_cs[3])
{
  double cloud_sz[2];
  double upper[2];
  double pos_cs[3];
  double pos_cs_n[2];
  ASSERT(sky && pos_ws && out_pos_cs);
  ASSERT(pos_ws[2] >= sky->htcp_desc.lower[2]);
  ASSERT(pos_ws[2] <= sky->htcp_desc.upper[2]);

  if(!sky->repeat_clouds) { /* Nothing to do */
    return d3_set(out_pos_cs, pos_ws);
  }

  if(!sky->repeat_clouds /* Nothing to do */
  || (  pos_ws[0] >= sky->htcp_desc.lower[0]
     && pos_ws[0] <= sky->htcp_desc.upper[0]
     && pos_ws[1] >= sky->htcp_desc.lower[1]
     && pos_ws[1] <= sky->htcp_desc.upper[1])) {
    return d3_set(out_pos_cs, pos_ws);
  }

  /* The cloud upper bound is not inclusive. Define the inclusive upper bound
   * of the cloud */
  upper[0] = nextafter(sky->htcp_desc.upper[0], sky->htcp_desc.lower[0]);
  upper[1] = nextafter(sky->htcp_desc.upper[1], sky->htcp_desc.lower[1]);
  cloud_sz[0] = upper[0] - sky->htcp_desc.lower[0];
  cloud_sz[1] = upper[1] - sky->htcp_desc.lower[1];

  /* Transform pos in normalize local cloud space */
  pos_cs_n[0] = (pos_ws[0] - sky->htcp_desc.lower[0]) / cloud_sz[0];
  pos_cs_n[1] = (pos_ws[1] - sky->htcp_desc.lower[1]) / cloud_sz[1];
  pos_cs_n[0] -= (int)pos_cs_n[0]; /* Get fractional part */
  pos_cs_n[1] -= (int)pos_cs_n[1]; /* Get fractional part */
  if(pos_cs_n[0] < 0) pos_cs_n[0] += 1;
  if(pos_cs_n[1] < 0) pos_cs_n[1] += 1;

  /* Transform pos in local cloud space */
  pos_cs[0] = sky->htcp_desc.lower[0] + pos_cs_n[0] * cloud_sz[0];
  pos_cs[1] = sky->htcp_desc.lower[1] + pos_cs_n[1] * cloud_sz[1];
  pos_cs[2] = pos_ws[2];

  ASSERT(pos_cs[0] >= sky->htcp_desc.lower[0]);
  ASSERT(pos_cs[0] <= sky->htcp_desc.upper[0]);
  ASSERT(pos_cs[1] >= sky->htcp_desc.lower[1]);
  ASSERT(pos_cs[1] <= sky->htcp_desc.upper[1]);

  return d3_set(out_pos_cs, pos_cs);
}

