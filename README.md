# High-Tune: Sky

This library loads and manages data representing a clear/cloudy sky. The
atmospheric gas mixture is loaded from a
[HTGOP](https://gitlab.com/meso-star/htgop) file while cloud properties are
loaded from data stored with respect to the
[HTCP](https://gitlab.com/meso-star/htcp/) fileformat. The optical properties
of the clouds are finally retrieved from a
[HTMie](https://gitlab.com/meso-star/htmie/) file. Once provided, the clouds
can be repeated infinitely into the X and Y dimension. 

HTSky relies onto the [Star-VX](https://gitlab.com/meso-star/star-vx) library
to build space partitioning data structures upon raw sky data. These structures
can then be used in conjunction of null-collision algorithms to accelerate the
tracking of a ray into this inhomogeneous medium, as described in [Villefranque
et al.
(2019)](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2018MS001602).
These accelerating structures are built with respect to an optical thickness
criterion whose threshold is user defined. One can also fix the maximum
resolution of the structures in order to constraint their memory consumption.
Even though the building itself of the structures is quite efficient, computing
their underlying data from the input files can be time consuming. So, once
built, these structures can be stored into a file to drastically speed up the
subsequent initialisation steps of the same sky. We point out that this file is
valid as long as the provided HTGOP, HTCP and HTMie files are the ones used to
build the cached structures. If not, an error is returned on sky creation.

## How to build

This library is compatible GNU/Linux 64-bits. It relies on the
[CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build. 
It also depends on the
[HTCP](https://gitlab.com/meso-star/htcp/),
[HTGOP](https://gitlab.com/meso-star/htgop/),
[HTMie](https://gitlab.com/meso-star/htmie/),
[RSys](https://gitlab.com/vaplv/rsys/) and
[Star-VX](https://gitlab.com/meso-star/star-vx/) libraries, and on
[OpenMP](http://www.openmp.org) 1.2 to parallelize its computations.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake documentation](https://cmake.org/documentation)
for further informations on CMake.

## Release notes

### Version 0.2.2

- Correction of compilation errors due to API breaks in Star-VoXel 0.2.
- Correction of invalid memory writes.

### Version 0.2.1

- Fix the acceleration data structures: the Kmin and Kmax stored in the
  hierarchical trees could be wrong for cloud fields with data irregularly
  structured along the Z axis.

### Version 0.2

- Make uniform the sky setup in shortwave and in longwave. On sky creation, the
  caller now defines the type of the spectral data to handle (i.e.  shortwave
  or longwave) and their wavelength range. The `double wlen_lw_range[2]` member
  variable of the `struct htsky_args` data structure is thus renamed in `double
  wlen_range[2]` and is used in both cases. Finally the new member variable
  `enum htsky_spectral_type spectral_type` defines the type of this spectral
  range.
- Add the `htsky_get_raw_spectral_bounds` function that returns the spectral
  range of the loaded sky data overlapped by the user define wavelength range.

### Version 0.1

- Add longwave support. Add the `double wlen_lw_range[2]` member
  variable to the `struct htsky_args` data structure that, once correctly
  defined, is used to setup the sky data for the provided long wave range. By
  default this range is degenerated meaning that the sky is setup for the short
  wave range [380, 780] nm.
- Add the `htsky_find_spectral_band` function: it returns the spectral band
  that includes the submitted wavelength.
- Remove the `htsky_sample_sw_spectral_data_CIE_1931_<X|Y|Z>` functions that
  explicitly rely on the CIE XYZ color space.
- Add the
  `htsky_fetch_per_wavelength_particle_phase_function_asymmetry_parameter` that
  returns the Henyey-Greenstein phase function parameter for a given
  wavelength.

## License

Copyright (C) 2018, 2019, 2020, 2021 [|Meso|Star](http://www.meso-star.com)
(<contact@meso-star.com>). Copyright (C) 2018, 2019 Centre National de la
Recherche Scientifique (CNRS), Université Paul Sabatier
(<contact-edstar@laplace.univ-tlse.fr>). HTSky is free software released
under the GPL v3+ license: GNU GPL version 3 or later. You are welcome to
redistribute it under certain conditions; refer to the COPYING file for
details.

